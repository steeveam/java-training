package readinput;

import java.util.Scanner;

public class ReadInput {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        if (num > 0) {
            for (int i = num; i > 0; i--)
                System.out.println(i);
        }
        if (num < 0) {
            for (int i = num; i < 0; i++)
                System.out.println(i);

        }

        System.out.println("Hello World!");
    }

}

