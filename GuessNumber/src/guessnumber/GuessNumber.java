package guessnumber;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        Scanner question = new Scanner(System.in);
        int withoutthis;
        int number;
        String answer;

        do {
            System.out.println("Zadaj cislo, na ktore nemam mysliet : ");
            withoutthis = question.nextInt();

            number = getRandomNumberExcept(withoutthis);

            System.out.println(number);

            System.out.println("Myslim si cislo, okrem cisla " + withoutthis);

            //uhadniCislo je volanie funkcie, number je parameter
            uhadniCislo(number);
            // urobit to cez do while.. do na zaciatku.. while nie
            String yes = "Ano";
            String no = "Nie";

            Scanner odpoved = new Scanner(System.in);

            System.out.println("Gratulujem, uhadli ste cislo! Chcete hrat znovu? (Ano/Nie)");
            answer = odpoved.nextLine();

        } while (!answer.equals("Nie"));

        // equals je funkcia porovnania objektov
    }


    private static void uhadniCislo(int number) {
        Scanner question = new Scanner(System.in);
        int num;

        do {
            System.out.println("Zadajte cislo :");

            // volanie funkcie nextInt nad objektom question
            num = question.nextInt();

            if (number != num)
                System.out.println("Neuhadli ste cislo, vyskusajte to znovu..");

        } while (number != num);
    }


    private static int getRandomNumberExcept(int withoutthis) {
        int number;
        Random generator = new Random();

        do {
            number = generator.nextInt(3);
        } while (withoutthis == number);

        return number;
    }
}
