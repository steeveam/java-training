package boom;

public class Boom {
    public static void main(String[] args) {
        for (int i = 1; i < 11; i++) {
            if (i == 3 || i == 5) {
                System.out.println("BOOM!");
            } else System.out.println(i);
        }
    }
}
